package kz.PvP.SkyWars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import kz.PvP.SkyWars.Commands.AdminCommands;
import kz.PvP.SkyWars.Commands.PlayerCommands;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Events.ChatListener;
import kz.PvP.SkyWars.Events.InteractListener;
import kz.PvP.SkyWars.Events.JoinListener;
import kz.PvP.SkyWars.Events.buildListener;
import kz.PvP.SkyWars.Events.deathListener;
import kz.PvP.SkyWars.Events.moveListener;
import kz.PvP.SkyWars.Methods.ChangeMaps;
import kz.PvP.SkyWars.Methods.Powerups;
import kz.PvP.SkyWars.Methods.ShopSystem;
import kz.PvP.SkyWars.Utilities.Files;
import kz.PvP.SkyWars.repeatingTasks.SWScoreboardUpdater;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;



public class Main extends JavaPlugin {



	public static Main plugin;
	static Logger log = Logger.getLogger("Minecraft");
	public static HashMap<String, arenaDetails> arenas = new  HashMap<String, arenaDetails>();
	public static HashMap<String, arenaDetails> freshArenas = new  HashMap<String, arenaDetails>();
	public static HashMap<String, arenaDetails> freshValidArenas = new  HashMap<String, arenaDetails>();
	public static HashMap<String, String> globalGamers = new  HashMap<String, String>();

	public static ArrayList<String> openArenas = new ArrayList<String>();
	public static int waitTime = 15;
	public static Location lobbyLoc = null;
	public static BukkitTask scoreboardUpdater = null;
	public static Integer killTokens = 1;

	public void onLoad(){
	}



	public void onEnable()
	{

		new Files(this);
		new ChangeMaps(this);

		setupArenas(this);

		new ChatListener(this);
		new InteractListener(this);
		new JoinListener(this);
		new deathListener(this);
		new moveListener(this);
		new buildListener(this);
		new ShopSystem(this);
		new Powerups(this);
		log.info("Enabling " + this.getName());
		getCommand("swa").setExecutor(new AdminCommands(this));
		getCommand("leave").setExecutor(new PlayerCommands(this));
		getCommand("join").setExecutor(new PlayerCommands(this));
		getCommand("shop").setExecutor(new PlayerCommands(this));
		for (Player p : Bukkit.getOnlinePlayers()){
			JoinListener.resetPlayerStatus(p);
			JoinListener.autoJoin(p,"Random");
		}
		scoreboardUpdater =  new SWScoreboardUpdater().runTaskTimerAsynchronously(this, 10L, 10L);
	}






	public static void setupArenas(Main mainPlugin) {
		FileConfiguration arenasConf = Files.arenas.getCustomConfig();

		for (World world : Bukkit.getServer().getWorlds())
			ChangeMaps.unloadMap(world.getName());

		if (arenasConf.contains("Lobby"))
			lobbyLoc = Files.getLoc("Lobby", Files.arenas);
		else
			lobbyLoc = mainPlugin.getServer().getWorlds().get(0).getSpawnLocation();

		for (String arenaName : arenasConf.getKeys(false)){
			//Message.C("Map: " + ArenaDetails.getMapName(), true);
			if (arenasConf.contains(arenaName + ".arenaMap"))
				ChangeMaps.LoadMapSave(arenasConf.getString(arenaName + ".arenaMap"));
		}

		//freshArenas.clear();

		for (String arena: arenasConf.getKeys(false)){
			ConfigurationSection arenaInfo = arenasConf.getConfigurationSection(arena);
			String subSection = "arenaMap";
			if (arenaInfo.contains(subSection)){
				arenaDetails details = new arenaDetails(mainPlugin);



				details.setMapName(arenaInfo.getString(subSection));


				if (arenaInfo.contains(subSection  = "spawnpoints"))
					for (String arenaTeleport : arenaInfo.getConfigurationSection(subSection).getKeys(false))
						details.addSpawnpoint(Files.getLoc(arena + "." + subSection + "." + arenaTeleport, Files.arenas).getBlock().getLocation().add(0.5,0,0.5));

				details.setName(arena);

				freshArenas.put(arena.toLowerCase(), details.clone());

				if (details.getSpawnpoints().size() >= 4){
					freshValidArenas.put(arena.toLowerCase(), details.clone());
				}
			}
		}


		for (arenaDetails detailsOriginal : freshValidArenas.values()){
			arenaDetails details = detailsOriginal.clone();
			openArenas.add(detailsOriginal.getName().toLowerCase());
			System.out.println("Setting up the map: "+ details.getMapName());
			for (Location loc : details.getSpawnpoints()){
				for (int x = -1; x <= 1; x++){
					for (int y = -1; y <= 2; y++){
						for (int z = -1; z <= 1; z++){
							if (x == 0 && z == 0 && (y == 0 || y == 1 || y == 2))
								loc.clone().add(x,y,z).getBlock().setType(Material.AIR);
							else{
								details.addDropper(loc.clone().add(x,y,z));
								loc.clone().add(x,y,z).getBlock().setType(Material.GLASS);
							}
						}
					}
				}
			}
			arenas.put(details.getName().toLowerCase(), details.clone());
		}
	}



	public void onDisable(){
		log.info("Disabling " + this.getName());
		for (World world : Bukkit.getServer().getWorlds())
			ChangeMaps.unloadMap(world.getName());
		log.info("Unloaded all worlds");

	}

	public static void refreshConfigEntries(Main plugin) {
		// TODO Auto-generated method stub
		FileConfiguration conf = Files.config.getCustomConfig();
		String ConfigSetting = "";

	}

}