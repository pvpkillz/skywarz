package kz.PvP.SkyWars.Utilities;


import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.logging.Logger;

import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.Config;
import kz.PvP.SkyWars.Enums.arenaDetails;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;


public class Files{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public static Config config;
	public static Config arenas;
	public static Config shop;

	public Files (Main mainclass){
		plugin = mainclass;

		config = new Config("config", plugin);
		arenas = new Config("arenas", plugin);
		shop = new Config("shop", plugin);
	}

	public static void saveArenas() {
		arenas.setEmptyConfig();
		ConfigurationSection arenaConf = arenas.getCustomConfig();
		for (Entry<String, arenaDetails> details : Main.freshArenas.entrySet()){
			arenaConf.set(details.getValue().getName() + ".arenaMap", details.getValue().getMapName());
			int count = 1;
			arenaConf.set(details.getValue().getName() + ".spawnpoints", new ArrayList<String>());

			for (Location loc : details.getValue().getSpawnpoints()){
				setLocation(arenas, loc, details.getValue().getName() + ".spawnpoints."+count);
				count++;
			}
		}
		arenas.saveCustomConfig();

	}

	public static Location getLoc(String string, Config files) {
		Location loc = null;
		FileConfiguration file = files.getCustomConfig();
		if (file.contains(string)){
			int x = file.getInt(string + ".X");
			int y = file.getInt(string + ".Y");
			int z = file.getInt(string + ".Z");
			String world = file.getString(string + ".World");
			
			
			loc = new Location(Bukkit.getWorld(world),x,y,z);

			if (file.contains(string + ".Yaw"))
				loc.setYaw((float)file.getDouble(string + ".Yaw"));
			if (file.contains(string + ".Pitch"))
				loc.setPitch((float)file.getDouble(string + ".Pitch"));
		}
		else
			loc = Bukkit.getWorlds().get(0).getSpawnLocation();
		return loc;
	}

	public static void setLocation(Config cf, Location loc, String locName) {
		cf.getCustomConfig().set(locName + ".X", loc.getX());
		cf.getCustomConfig().set(locName + ".Y", loc.getY());
		cf.getCustomConfig().set(locName + ".Z", loc.getZ());
		cf.getCustomConfig().set(locName + ".World", loc.getWorld().getName());
		cf.getCustomConfig().set(locName + ".Yaw", loc.getYaw());
		cf.getCustomConfig().set(locName + ".Pitch", loc.getPitch());
	}

}