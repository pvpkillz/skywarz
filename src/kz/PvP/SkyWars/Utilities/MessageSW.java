package kz.PvP.SkyWars.Utilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.arenaDetails;

public class MessageSW {

	public static final String GameStarting = "&cGame starting in &6%time.";
	public static final String GameStarted = "&aGame has started. Good luck!";
	public static final String Eliminated = "&a%player &7has been eliminated.";
	public static final String GameEndedWinner = "&6%arena &7ended with &a%player&7 as the winner.";
	public static final String WonTheGame = "%winner won a round on %map.";
	public static final String EditingMap = "You are now editing %map map.";
	public static final String SettingUpNewMap = "You are now setting up a map, for a new map enter it's name, else choose name from below.";
	public static final String CreatedNewMap = "You have created a new map entry with the name %map.";
	public static final String ChooseArenaName = "Type an arena name to edit. To list them, type 'list'.";
	public static final String SpawnpointSelector = "&7&m----&a Spawnpoint Selector &7&m----";
	public static final String ChestSelector = "&7&m----&a Chest Selector &7&m----";
	public static final String DropperSelector = "&7&m----&a Dropper Selector &7&m----";
	public static final String GrapplingHook = "&aGrappling Hook";
	public static final String UsedGrapplingHook = "You used the grappling hook.";
	public static final String GrapplingHooked = "Grappling Hook has been hooked.";
	public static final String SlowballName = "Slowball";
	public static final String KnockbombName = "Knockbomb";
	public static final String HoleName = "Hole";
	public static String deletedPreviousEntry = "&cDeleted &7previous entry.";
	public static String addedEntry = "&aAdded &7new entry.";
	public static String arenaStatuses = "&7Arena statuses:";
	public static String incorrectSyntax = "&cIncorrect syntax.";
	public static String noMoreEntriesLeft = "&cNo more entries left to remove.";
	public static String joinedTheArena = "&6%player &7has joined the arena.";
	public static String noArenas = "No open arenas found. Your queued for next open game.";
	public static String needMorePlayers = "&cMore players are needed to start a game.";
	public static String notInArena = "&cYou are not in an arena. &7Type &a/j&7 to join a random arena.";
	public static String alreadyInArena = "&cAlready in arena. &7Type &c/leavel&6 to leave.";
	public static String arenaDoesNotExist = "Arena does not exist. Valid:";
	public static String arenaStartedAlready = "Arena already started.";
	public static String toggledAutoRejoin = "&7Rejoin arenas automatically: &6%status";
	public static String inviteMoreForReward = "More people need to be in SkyWars, to enable win rewards.";
	public static String purchasedPowerup = "&aYou have purchased a power up.";
	public static String rackUpShop = "&bMake sure to rack up on supplies to win in the shop using &a/shop";



	public MessageSW(){
		Message.PREFIX = "&aSky&6Wars";
	}



	public static void AP(String arenaName, String msg, boolean IncludePrefix){
		if (Main.arenas.containsKey(arenaName.toLowerCase())){
			arenaDetails details = Main.arenas.get(arenaName.toLowerCase());
			for (String p : details.getPlayers()){
				Player player = Bukkit.getPlayer(p);
				if (player != null)
					Message.P(player, msg, IncludePrefix);
			}
		}

	}
























	public static void printHelpMenu(Player p, String type) {
		if (type.equalsIgnoreCase("adminMenu")){
			Message.P(p, Message.Replacer(Message.HeaderMenu, "SkyWars", "PkzAPI"), false);
			Message.P(p, " &7 - /swa setup &8 | &7 Set-up a new or old arena", false);
			Message.P(p, " &7 - /swa list &8 | &7 View arenas and statuses", false);
			Message.P(p, " &7 - /swa force <Name> &8 | &7 Force end/start arenas", false);

		}
	}


}
