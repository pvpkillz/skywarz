package kz.PvP.SkyWars.Methods;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

import kz.PvP.SkyWars.Main;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public class ChangeMaps implements Listener {
	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	
	public ChangeMaps (Main mainclass) {
		plugin = mainclass;
	}
	
	public static void unloadMap(String mapname) {
		if(plugin.getServer().unloadWorld(mapname, false)) {
			plugin.getLogger().info("Successfully unloaded " + mapname);
		} else {
			plugin.getLogger().severe("COULD NOT UNLOAD " + mapname);
		}
	}
	
	
	//Loading maps (MUST BE CALLED AFTER UNLOAD MAPS TO FINISH THE ROLLBACK PROCESS)
	private static void copyDirectory(File sourceLocation, File targetLocation)
				throws IOException {
		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}
			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
										targetLocation, children[i]));
			}
		} else {
			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}
	public static void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static boolean deleteDirectory(File path) {
		if( path.exists() ) {
			File files[] = path.listFiles();
			for (int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
				//end else
			}
		}
		return( path.delete() );
	}
	public static void BrandNewWorld(final String worldName) {
		WorldCreator world = new WorldCreator(worldName).type(WorldType.NORMAL);
		World newWorld = world.createWorld();
		newWorld.setDifficulty(Difficulty.NORMAL);
		newWorld.setAutoSave(false);
		newWorld.setFullTime(0);
		newWorld.setKeepSpawnInMemory(true);
		newWorld.setPVP(true);
		newWorld.setStorm(false);
		newWorld.setThundering(false);
		newWorld.loadChunk(newWorld.getChunkAt(newWorld.getSpawnLocation()));
		plugin.getLogger().info("World created '" + worldName + "'");
	}
	
	public static void LoadMapSave(String mapname) {
		World world = plugin.getServer().getWorld(mapname);
		if (world != null) {
			for ( Entity e : world.getEntities()) {
				if (e.getType() != EntityType.PLAYER)
				    			e.remove();
			}
			for (Player p : world.getPlayers())
				p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
			if(plugin.getServer().unloadWorld(mapname, false)) {
				plugin.getLogger().info(mapname + " Unloaded.");
				deleteDirectory(world.getWorldFolder());
				LoadMapSave(mapname);
			} else
				plugin.getLogger().info("Failed to unload map '" + mapname + "'");
		} else {
			plugin.getLogger().info("Copying map '" + mapname + "'");
			try {
				copyDirectory(new File(plugin.getDataFolder(), mapname), new File(mapname));
				plugin.getLogger().info("Copied map '" + mapname + "'");
				BrandNewWorld(mapname);
			}
			catch (IOException e) {
				plugin.getLogger().warning("Error: " + e.toString());
			}
		}
	}
	
	
	
	public static Location getRandomLocation(World world) {
		Location startFrom = world.getSpawnLocation();
		Location loc;
		loc = startFrom.clone();
		int newY = world.getHighestBlockYAt(loc);
		loc.setY(newY);
		return loc;
	}
}