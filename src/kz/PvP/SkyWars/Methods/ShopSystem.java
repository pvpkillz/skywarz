package kz.PvP.SkyWars.Methods;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import kz.PvP.PkzAPI.Commands.PlayerCommands;
import kz.PvP.PkzAPI.enums.IconInfo;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.IconMenu;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.shopItem;
import kz.PvP.SkyWars.Utilities.Files;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ShopSystem {
	public static HashMap<String, ArrayList<shopItem>> shop = new HashMap<String, ArrayList<shopItem>>();

	public static Main plugin;


	public ShopSystem(Main main) {
		plugin = main;
	}

	public static void openShop (final Player p, final String menuName){
		FileConfiguration config = Files.shop.getCustomConfig();
		String Title = menuName;
		int size = 0;

		if (menuName.isEmpty() || menuName.equalsIgnoreCase("global"))
			Title = "Main Menu -> Catagories";
		else
			size++;
		size = config.getStringList(menuName).size() + size;


		int invsize = size;



		if (size  % 9 == 0) 
			invsize = size;
		else 
			invsize = size + (9 - (size % 9));


		if (PlayerCommands.MENUS.containsKey(p.getName())){
			PlayerCommands.MENUS.get(p.getName()).getMenu().destroy();
			PlayerCommands.MENUS.remove(p.getName());
		}
		checkMenu(menuName);
		IconMenu menu = new IconMenu(ChatColor.GOLD + Title, p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
			public void onOptionClick(final IconMenu.OptionClickEvent event) {

				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						// What happens when the guy clicked on dis shit
						boolean diffShop = false;
						if (!(menuName.equalsIgnoreCase("global") || menuName.isEmpty())){
							if (event.getPosition() == 0){
								diffShop = true;
								openShop(p, "global");
							}
						}
						if (diffShop == false){
							checkMenu(ChatColor.stripColor(event.getName()));
							if (shop.containsKey(ChatColor.stripColor(event.getName())))
								openShop(p, ChatColor.stripColor(event.getName()));
							else
								purchaseItem(p, menuName, event.getPosition());	
						}
					}
				}, 2L);
				event.setWillClose(true);
				event.setWillDestroy(false);
			}
		}, plugin);

		menu = getShopContents(menuName, menu, p);

		if (!(menuName.equalsIgnoreCase("global") || menuName.isEmpty())){
			ItemStack isMainMenu = new ItemStack(Material.STAINED_GLASS_PANE, 1);
			isMainMenu.setDurability((short) 15);
			menu.setOption(0, isMainMenu, Message.Colorize("&8Return to &7Main Menu"));
		}

		InventoryView view = menu.open(p);
		PlayerCommands.MENUS.put(p.getName(), new IconInfo(menu, Title, view));
	}

	private static  void checkMenu(String menuName) {
		FileConfiguration config = Files.shop.getCustomConfig();

		if (!shop.containsKey(menuName) && config.contains(menuName)){
			shop.put(menuName, new ArrayList<shopItem>());

			for (String confLine : config.getStringList(menuName)){
				String itemLine = null;
				String itemPrice = null;
				Integer price = null;
				Integer pos = null;
				String itemPos = null;
				String itemDesc = null;
				String itemName = null;
				if (confLine.contains(";")){
					String[] splitUp = confLine.split(";");
					if (splitUp.length == 2){
						if (!splitUp[0].isEmpty())
							itemLine = splitUp[0];
						if (!splitUp[1].isEmpty())
							itemPrice = splitUp[1];
					}
					else if (splitUp.length == 3){
						if (!splitUp[0].isEmpty())
							itemLine = splitUp[0];
						if (!splitUp[1].isEmpty())
							itemPrice = splitUp[1];
						if (!splitUp[2].isEmpty())
							itemName = splitUp[2];
					}
					else if (splitUp.length == 4){
						if (!splitUp[0].isEmpty())
							itemLine = splitUp[0];
						if (!splitUp[1].isEmpty())
							itemPrice = splitUp[1];
						if (!splitUp[2].isEmpty())
							itemName = splitUp[2];
						if (!splitUp[3].isEmpty())
							itemPos = splitUp[3];
					}
					else if (splitUp.length >= 5){
						if (!splitUp[0].isEmpty())
							itemLine = splitUp[0];
						if (!splitUp[1].isEmpty())
							itemPrice = splitUp[1];
						if (!splitUp[2].isEmpty())
							itemName = splitUp[2];
						if (!splitUp[3].isEmpty())
							itemPos = splitUp[3];
						if (!splitUp[4].isEmpty())
							itemDesc = splitUp[4];
					}
				}
				else
					itemLine = confLine;

				ItemStack is = itemModifications.getItemInfo(itemLine, null);        	

				shopItem shopItem = new shopItem();
				if (itemName == null)
					itemName = Message.CleanCapitalize(is.getType().name());
				else if (is.hasItemMeta() && is.getItemMeta().hasDisplayName() &&  (itemName.equalsIgnoreCase("name")))
					itemName = is.getItemMeta().getDisplayName();


				if (ConvertTimings.isInteger(itemPrice) && !itemPrice.isEmpty())
					price = Integer.parseInt(itemPrice);

				if (ConvertTimings.isInteger(itemPos) && !itemPos.isEmpty())
					pos = Integer.parseInt(itemPos);


				shopItem.setDesc(itemDesc);
				shopItem.setPrice(price);
				shopItem.setItemStack(is);
				shopItem.setName(itemName);
				shopItem.setPosition(pos);
				shop.get(menuName).add(shopItem);
			}
		}
	}

	public static IconMenu getShopContents(String menuName, IconMenu menu, Player p){
		ArrayList<Integer> usedPositions = new ArrayList<Integer>();
		int mainPos = 0;
		if (menuName.isEmpty() || menuName.equalsIgnoreCase("global"))
			mainPos = 0;
		else
			mainPos = 1;


		int pos = mainPos;
		for (shopItem shopItem : shop.get(menuName)){
			int position = mainPos;
			if (shopItem.getPosition() == null){
				while (usedPositions.contains(pos))
					pos++;
				position = pos;
			}
			else{
				position = shopItem.getPosition();
				while (usedPositions.contains(position))
					position++;				
			}
			usedPositions.add(position);
			String description = shopItem.getDesc();
			String[] splitupDesc = description.split(" ");

			int totalLineLength = 0;
			ArrayList<String> newInfoLine = new ArrayList<String>();
			StringBuilder lineCur = new StringBuilder();
			for (int x = 0; x <= splitupDesc.length - 1; x++){
				String word = splitupDesc[x];
				word = Message.Colorize(word);
				if ((totalLineLength + word.length()) >= 40 || x >= splitupDesc.length - 1){
					if ((totalLineLength + word.length()) >= 40)
						x--;
					else if (x >= splitupDesc.length - 1){
						if (lineCur.toString().isEmpty())
							lineCur.append(word);
						else
							lineCur.append(" " + word);
					}
					totalLineLength = 0;					

					newInfoLine.add(lineCur.toString());
					lineCur =  new StringBuilder();
				}
				else{
					if (lineCur.toString().isEmpty())
						lineCur.append(word);
					else
						lineCur.append(" " + word);

					totalLineLength += word.length();
				}
			}

			String[] descList = null;


			if (shopItem.getPrice() > 0){
				newInfoLine.add(" ");
				if (Stats.getTokens(p.getName()) >= shopItem.getPrice())
					newInfoLine.add(ChatColor.GREEN + "Cost: " + shopItem.getPrice() + " tokens");
				else
					newInfoLine.add(ChatColor.RED + "Cost: " + shopItem.getPrice() + " tokens");

				descList = new String[newInfoLine.size() + 2];
			}
			else
				descList = new String[newInfoLine.size()];
			shop.get(menuName).get(shop.get(menuName).indexOf(shopItem)).setPosition(position);
			menu.setOption(position, shopItem.getItem(), Message.Colorize(shopItem.getName()), newInfoLine.toArray(descList));
		}
		return menu;
	}

	public static void purchaseItem(Player p, String shopName, int position) {
		for (shopItem shopItem : shop.get(shopName)){
			if (shopItem.getPosition().equals(position)){
				// We do the payment processing here...

				try {
					if (Mysql.modifyTokens(p.getName(), -1 * shopItem.getPrice(), true)){
						Message.P(p, MessageSW.purchasedPowerup , true);
						ItemStack is = shopItem.getItem();
						ItemMeta im = is.getItemMeta();
						im.setLore(new ArrayList<String>());
						is.setItemMeta(im);
						p.getInventory().addItem(is);
					}
						
				} catch (SQLException e) {
					Message.P(p, Message.failedTransaction, true);
				}


			}
		}

	}

}
