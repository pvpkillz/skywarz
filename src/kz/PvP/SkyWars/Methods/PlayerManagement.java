package kz.PvP.SkyWars.Methods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import java.util.Map.Entry;

import kz.PvP.PkzAPI.enums.PlayerScoreboard;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Events.JoinListener;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

public class PlayerManagement {


	public static ArrayList<UUID> mapEditors = new ArrayList<UUID>();
	public static HashMap<UUID, String> mapEditiorLink = new HashMap<UUID, String>();
	public static LinkedList<String> queuedPlayers = new LinkedList<String>();


	public static void giveEditorTools(Player p) {
		PlayersInfo.ClearFully(p);
		p.setFireTicks(0);
		p.setFoodLevel(20);
		p.setHealth(20.0);
		p.setExp(0);
		p.setTotalExperience(0);
		p.setLevel(0);
		itemModifications.giveItem(p, MessageSW.SpawnpointSelector, Material.IRON_HOE, 3, "&7Use this tool to\nchoose spawnpoints.", false);
	}



	public static void displayArenasList(Player p) {
		for (Entry<String, arenaDetails> details : Main.freshArenas.entrySet()){
			StringBuilder valid = new StringBuilder();
			valid.append("   &7- " + details.getKey());
			if (Main.freshValidArenas.containsKey(details.getKey()))
				valid.append(" &7 | &aActive");
			Message.P(p,  valid.toString(), false);
		}
	}

	public static String getPlayerArena(String p){
		if (Main.globalGamers.containsKey(p))
			return Main.globalGamers.get(p);
		return null;
	}

	public static boolean isPlayerGamer(String p){
		String arenaName = getPlayerArena(p);
		if (arenaName == null)
			return false;
		else
			return true;
	}



	public static void removePlayerEntries(Player p) {
		String arenaName = getPlayerArena(p.getName());
		if (arenaName != null)
			Main.arenas.get(arenaName).removePlayer(p.getName(), false);

		mapEditors.remove(p.getUniqueId());
		mapEditiorLink.remove(p.getUniqueId());
		queuedPlayers.remove(p.getName());
		Main.globalGamers.remove(p.getName());
	}



	public static void setupQueue() {
		for (String playerName : queuedPlayers){
			boolean joined = JoinListener.autoJoin(PlayersInfo.getPlayer(playerName), "Random");
			if (joined == true){
				queuedPlayers.remove(playerName);
			}
		}
	}


}
