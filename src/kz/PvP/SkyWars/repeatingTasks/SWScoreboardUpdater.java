package kz.PvP.SkyWars.repeatingTasks;

import java.util.ArrayList;

import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Methods.PlayerManagement;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;




public class SWScoreboardUpdater extends BukkitRunnable {
	ArrayList<String> previousMapsScoreboard = new ArrayList<String>();


	public SWScoreboardUpdater() {
		
	}

	@Override
	public void run() {
		ArrayList<String> mapsScoreboard = new ArrayList<String>();
		for (String arenaName : Main.openArenas)
			mapsScoreboard.add("&7"+ Main.freshArenas.get(arenaName.toLowerCase()).getMapName() + "//" +  Main.arenas.get(arenaName.toLowerCase()).getPlayers().size());


		for (Player p : Bukkit.getOnlinePlayers()){
			if (Scoreboards.scoreboardsList.containsKey(p.getName())){
				//if (mapsScoreboard.size() != previousMapsScoreboard.size())
				//	JoinListener.scoreboardsList.remove(p.getName());
				ArrayList<String> listScoreboard = new ArrayList<String>();
				if (!PlayerManagement.isPlayerGamer(p.getName())){
					Scoreboards.updateScoreboardEntries(p, mapsScoreboard, Message.Colorize("&aOpen arenas:"));
				}
				else{
					String arenaName = PlayerManagement.getPlayerArena(p.getName());
					arenaDetails arena = Main.arenas.get(arenaName);
					for (String pName : arena.getOriginal()){
						if (arena.getPlayers().contains(pName))
							listScoreboard.add("&7"+ pName + "//killstreak");
						else
							listScoreboard.add("&7"+pName + "//-1");
					}
					Scoreboards.updateScoreboardEntries(p, listScoreboard, Message.Colorize("&a&lLeaderboard"));
				}
				previousMapsScoreboard = mapsScoreboard;
			}
		}
	}
}
