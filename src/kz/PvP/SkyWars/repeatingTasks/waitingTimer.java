package kz.PvP.SkyWars.repeatingTasks;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.scheduler.BukkitRunnable;




public class waitingTimer extends BukkitRunnable {
	private String arenaName = "";

	public waitingTimer(String ArenaName) {
		arenaName = ArenaName;
	}

	@Override
	public void run() {
		arenaDetails details = Main.arenas.get(arenaName);
		Integer time = details.getWaitTime() - 1;
		
		if (details.getSpawnpoints().size() == 0 && time > 5)
			time = 5;
		
		
		
		Main.arenas.get(arenaName).setWaitTime(time);
		if (time >= 1){// Still counting down
				
				
			if (time <= 3)
				MessageSW.AP(arenaName, Message.Replacer(MessageSW.GameStarting, ConvertTimings.convertTime(time), "%time"), true);
			
			
			
		}
		else{// Finished counting down. Lets start this...
			if (details.getPlayers().size() >= 2){
				Main.arenas.get(arenaName.toLowerCase()).gameStarted();
				this.cancel();
			}
			else{
				MessageSW.AP(arenaName, MessageSW.needMorePlayers, true);
				details.setWaitTime(Main.waitTime);
			}
		}
		for (String pl : details.getPlayers()){
			PlayersInfo.getPlayer(pl).setLevel(time);
		}
	}
}
