package kz.PvP.SkyWars.Commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;










import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.UUIDFetcher;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.GameStatus;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Methods.ChangeMaps;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import kz.PvP.SkyWars.Methods.ShopSystem;
import kz.PvP.SkyWars.Utilities.Files;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;





public class AdminCommands implements CommandExecutor{

	public Main plugin;


	public AdminCommands(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("swa")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (p.hasPermission("pkz.skywars.*") || p.hasPermission("pkz.skywars.adminmenu") || p.isOp()){
					if (args.length >= 1){
						//We check what type of thing they want to do.
						String subMenu = args[0];
						if (subMenu.equalsIgnoreCase("setup")){
							// Setup new arena
							Message.P(p, MessageSW.SettingUpNewMap, true);							
							PlayerManagement.mapEditors.add(p.getUniqueId());
							PlayerManagement.displayArenasList(p);
							PlayerManagement.giveEditorTools(p);
						}
						else if (subMenu.equalsIgnoreCase("reload")){
							Files.shop.reloadCustomConfig();
							ShopSystem.shop.clear();
						}
						else if (subMenu.equalsIgnoreCase("list")){
							// List out the arenas there are, show also Raw arenas which need set-up still
							PlayerManagement.displayArenasList(p);
						}
						else if (subMenu.equalsIgnoreCase("loadWorld")){
							// List out the arenas there are, show also Raw arenas which need set-up still
							if (args.length >= 2){
								ChangeMaps.LoadMapSave(args[1]);
								p.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
							}
								
						}
						else if (subMenu.equalsIgnoreCase("setLobby")){
							// List out the arenas there are, show also Raw arenas which need set-up still
							PlayerManagement.mapEditors.add(p.getUniqueId());
							PlayerManagement.mapEditiorLink.put(p.getUniqueId(), "lobby");
							PlayerManagement.giveEditorTools(p);
						}
						else if (subMenu.equalsIgnoreCase("force")){
							// Force end/start round (Most likely only start tho)
							boolean failed = false;
							if (args.length >= 2){
								String arenaName = args[1];
								if (Main.arenas.containsKey(arenaName)){
									// We found the arena...
									if (Main.arenas.get(arenaName).getGameStatus().equals(GameStatus.STARTED)){
										Main.arenas.get(arenaName).gameEnded();
									}
									else
										Main.arenas.get(arenaName).setWaitTime(1);
								}
								else
									failed = true;
							}
							if (failed == true){
								// List out the valid arenas + statuses.
								Message.P(p, MessageSW.arenaStatuses, false);
								for (Entry<String, arenaDetails> details : Main.arenas.entrySet()){
									Message.P(p, "   - " + details.getKey() + " &7 | " + details.getValue().getGameStatus().name(), false);
								}
							}
						}
						else
							MessageSW.printHelpMenu(p, "adminMenu");
					}
					else
						MessageSW.printHelpMenu(p, "adminMenu");
				}
				else
					Message.P(p, Message.NoPermission, true);
			}
			else
				Message.C(Message.NotHumanlyPossible,true);

			return true;
		}
		return false;
	}


}
