package kz.PvP.SkyWars.Commands;

import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Events.JoinListener;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import kz.PvP.SkyWars.Methods.ShopSystem;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;




public class PlayerCommands implements CommandExecutor{

	public static Main plugin;


	public PlayerCommands(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("leave")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (PlayerManagement.isPlayerGamer(p.getName())){
					Main.arenas.get(PlayerManagement.getPlayerArena(p.getName())).removePlayer(p.getName(), false);
				}
				else
					Message.P(p, MessageSW.notInArena, true);
			}
			else
				Message.C(Message.NotHumanlyPossible, true);
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("join")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (!PlayerManagement.isPlayerGamer(p.getName())){
					if (args.length >= 1){
						// Specific arena request.
						JoinListener.autoJoin(p, args[0]);
					}
					else
						JoinListener.autoJoin(p, "Random");
				}
				else
					Message.P(p, MessageSW.alreadyInArena, true);
			}
			else
				Message.C(Message.NotHumanlyPossible, true);
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("shop")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (PlayerManagement.isPlayerGamer(p.getName())){
					/*
					 * Working on this shiz! :D
					 */
					ShopSystem.openShop(p, "global");
				}
				else
					Message.P(p, MessageSW.notInArena, true);
			}
			else
				Message.C(Message.NotHumanlyPossible, true);
			return true;
		}
		return false;
	}
}
