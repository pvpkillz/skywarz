package kz.PvP.SkyWars.Events;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import kz.PvP.PkzAPI.methods.Locations;
import kz.PvP.PkzAPI.methods.itemModifications;
import kz.PvP.PkzAPI.methods.serverPinging.ServerSignManagement;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import kz.PvP.SkyWars.Methods.Powerups;
import kz.PvP.SkyWars.Utilities.Files;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.MetadataValueAdapter;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

public class InteractListener implements Listener{
	public static Main plugin;
	public static ArrayList<String> freezes = new ArrayList<String>();

	public InteractListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	@EventHandler
	public void OnInteractEvent (PlayerInteractEvent e) throws SQLException{
		Player p = e.getPlayer();
		if (PlayerManagement.mapEditors.contains(p.getUniqueId())){
			// The guy is an editor...
			if (PlayerManagement.mapEditiorLink.containsKey(p.getUniqueId())){
				// The guy is all ready for setting up an arena...
				arenaDetails details = new arenaDetails(plugin);
				if (Main.freshArenas.containsKey(PlayerManagement.mapEditiorLink.get(p.getUniqueId()))){
					// Edit old map
					details = Main.freshArenas.get(PlayerManagement.mapEditiorLink.get(p.getUniqueId()));
				}
				ItemStack hand = p.getItemInHand();
				if (itemModifications.compareItems(hand, Material.IRON_HOE, MessageSW.SpawnpointSelector)){
					if (PlayerManagement.mapEditiorLink.containsKey(p.getUniqueId())){
						if (PlayerManagement.mapEditiorLink.get(p.getUniqueId()).equalsIgnoreCase("lobby")){
							Files.setLocation(Files.arenas, e.getClickedBlock().getLocation().add(0, 3, 0), "Lobby");
							Main.lobbyLoc = e.getClickedBlock().getLocation().add(0,3,0);
							Message.P(p, MessageSW.addedEntry, true);
						}
						else{
							if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)){
								// Delete previous entry...
								ArrayList<Location> newList = details.getSpawnpoints();
								if (newList.size() >= 1){
									newList.remove(newList.size() - 1);
									details.setSpawnpoints(newList);
									Message.P(p, MessageSW.deletedPreviousEntry, true);
								}
								else
									Message.P(p, MessageSW.noMoreEntriesLeft, true);
							}
							else if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
								// Create new entry...
								details.addSpawnpoint(e.getClickedBlock().getLocation().add(0,10,0));
								Message.P(p, MessageSW.addedEntry, true);
							}
							Main.freshArenas.put(PlayerManagement.mapEditiorLink.get(p.getUniqueId()), details);
							Files.saveArenas();
						}
						e.setCancelled(true);
					}
				}

			}
			else
				Message.P(p, MessageSW.ChooseArenaName, true);
		}
		else if (interactedWithSpecialSign(p, e.getClickedBlock())){
			e.setCancelled(true);
		}
		ItemStack hand = p.getItemInHand();
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
			if (itemModifications.compareItems(hand, Material.SLIME_BALL, MessageSW.SlowballName)){
				Snowball snowball = launchDropSnowball(p, 1.5);
				if (snowball != null)
					snowball.setMetadata("Special", new FixedMetadataValue(plugin, MessageSW.SlowballName));
			}
			if (itemModifications.compareItems(hand, Material.EGG, MessageSW.KnockbombName)){
				Snowball snowball = launchDropSnowball(p, 1.5);
				if (snowball != null){
					snowball.setMetadata("Special", new FixedMetadataValue(plugin, MessageSW.KnockbombName));

					//Powerups.launchedSpecials.put(p.getName(), "Egg/" + MessageSW.KnockbombName);

					e.setCancelled(true);
				}
			}
		}
	}
	/*
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent e) {
		if (e.getEntity() instanceof Snowball){
			Message.G("Launched a snowball", false);
			Snowball snowball = (Snowball) e.getEntity();
			Message.G("shoioter"  + snowball.getShooter(), false);
			if (snowball.getShooter() instanceof Player){
				Player p = (Player) snowball.getShooter();
				Message.G("Launched a special", false);
				if (Powerups.launchedSpecials.containsKey(p.getName())){
					if (Powerups.launchedSpecials.get(p.getName()).equalsIgnoreCase("Egg/" + MessageSW.KnockbombName)){
						Powerups.launchedPowerups.put(snowball.getUniqueId(), MessageSW.KnockbombName);
					}
					if (Powerups.launchedSpecials.get(p.getName()).equalsIgnoreCase("Slimeball/" + MessageSW.SlowballName)){
						Powerups.launchedPowerups.put(snowball.getUniqueId(), MessageSW.SlowballName);
					}
				}
			}
		}
		Message.G("Launched a normal", false);
	}
	 */
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent e){
		if (e.getEntity() instanceof Arrow){
			Arrow arrow = (Arrow) e.getEntity();
			if (arrow.getShooter() instanceof Player){
				Player p = (Player) arrow.getShooter();
				for (ItemStack is : p.getInventory().getContents()){
					if (is != null && is.getType() == Material.ARROW){
						if (is.hasItemMeta()){
							if (is.getItemMeta().hasDisplayName()){
								String arrowName = ChatColor.stripColor(is.getItemMeta().getDisplayName());
								if (arrowName.equalsIgnoreCase("Fire Arrow")){
									arrow.setFireTicks(20 * 10);
								}
								else if (arrowName.equalsIgnoreCase("Freeze Arrow")){
									FixedMetadataValue r = new FixedMetadataValue(plugin,"Freeze");

									arrow.setMetadata("Special", new FixedMetadataValue(plugin,"Freeze"));
								}
								else if (arrowName.equalsIgnoreCase("Poison Arrow")){
									arrow.setMetadata("Special", new FixedMetadataValue(plugin,"Poison"));
								}
								else if (arrowName.equalsIgnoreCase("Extra Knockback Arrow")){
									arrow.setKnockbackStrength(arrow.getKnockbackStrength() + 3);
								}
								else if (arrowName.equalsIgnoreCase("Hole Dig Arrow")){
									//Powerups.launchedSpecials.put(p.getName(), "HoleDigArrow/" + arrow.getUniqueId());
									arrow.setMetadata("Special", new FixedMetadataValue(plugin,"Hole"));
								}
							}
						}
						break;
					}
				}
			}
		}
	}



	@EventHandler
	public void onProjectileHit(ProjectileHitEvent e) {
		if (e.getEntity().hasMetadata("Special")){
			String special = e.getEntity().getMetadata("Special").get(0).asString();
			if (special.equalsIgnoreCase(MessageSW.KnockbombName)){
				e.getEntity().getLocation().getWorld().createExplosion(e.getEntity().getLocation().getX(), e.getEntity().getLocation().getY(), e.getEntity().getLocation().getZ(), 2f, false, false);
				HashMap<Player, Double> players = getPlayersAround(e.getEntity().getLocation(), 7);
				for (Entry<Player, Double> p : players.entrySet()){
					Vector unitVector = e.getEntity().getLocation().toVector().subtract(p.getKey().getLocation().toVector()).normalize();
					// Set speed and push entity:
					double distVal = 0.04 * (7 - p.getValue());
					p.getKey().setVelocity(unitVector.multiply(-1 + distVal));
				}
			}
			else if (special.equalsIgnoreCase(MessageSW.SlowballName)){
				e.getEntity().getLocation().getWorld().createExplosion(e.getEntity().getLocation().getX(), e.getEntity().getLocation().getY(), e.getEntity().getLocation().getZ(), 2f, false, false);
				HashMap<Player, Double> players = getPlayersAround(e.getEntity().getLocation(), 8);
				for (Entry<Player, Double> p : players.entrySet()){
					p.getKey().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 60, 0));
				}
			}
			else if (special.equalsIgnoreCase(MessageSW.HoleName)){
				e.getEntity().getLocation().getWorld().createExplosion(e.getEntity().getLocation().getX(), e.getEntity().getLocation().getY(), e.getEntity().getLocation().getZ(), 2f, false, false);
				ArrayList<Location> locations = getBlocksInRadius(getBlockLoc(e.getEntity().getLocation()), 5, 0);
				for (Location loc : locations){
					Block block = loc.getBlock();
					block.setType(Material.WOOL);
					Wool wool = (Wool) loc.getBlock().getState().getData();
					wool.setColor(DyeColor.BLACK);
					loc.getBlock().getState().update();


					block.setMetadata("Special", new FixedMetadataValue(plugin, "Hole"));
				}
			}
		}
	}

	private Location getBlockLoc(Location location) {
		if (location.clone().add(0,1,0).getBlock().getType() == Material.AIR)
			return location.clone().subtract(0,1,0).getBlock().getLocation();
		else
			return location.clone().add(0,1,0).getBlock().getLocation();
	}

	public ArrayList<Location> getBlocksInRadius(Location loc, Integer Radius, Integer Height){
		ArrayList<Location> list = new ArrayList<Location>();
		for (int i = -Radius; i <= Radius; i++) {
			for (int j = -Radius; j <= Radius; j++) {
				Location temp = loc.clone().add(i, 0, j);
				if (temp.distance(loc) < Radius) {
					if (Height >= 1){
						for (int k = 0; k <= Height; k++) {
							Location heightBlock = temp.clone().add(0, k, 0);
							if (heightBlock.getBlock().getType() != Material.AIR)
								list.add(heightBlock);
						}
					}
					else{
						if (temp.getBlock().getType() != Material.AIR){
							System.out.println("Type: " + temp.getBlock().getType().name());
							list.add(temp);
						}
					}
				}
			}
		}
		return list;
	}


	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if (!locationSameAs(e.getTo(), e.getFrom())){
			if (freezes.contains(p.getName()))
				e.setTo(e.getFrom());

			if (p.getLocation().add(0, -1, 0).getBlock().hasMetadata("Special")){
				if (p.getLocation().add(0, -1, 0).getBlock().getMetadata("Special").get(0).asString().equalsIgnoreCase("Hole")){
					p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 10, 0));
				}
			}

		}
	}

	public static boolean locationSameAs(Location pointShould, Location pointGot){
		if (pointShould.getBlockX() == pointGot.getBlockX() &&
				pointShould.getBlockZ() == pointGot.getBlockZ())
			return true;
		else
			return false;
	}


	@EventHandler
	public void onProjectileHurt (EntityDamageByEntityEvent e){
		if (e.getDamager() instanceof Arrow){
			Arrow arrow = (Arrow) e.getDamager();
			if (arrow.getShooter() instanceof Player){
				Player shooter = (Player) arrow.getShooter();
				if (e.getEntity() instanceof Player){
					final Player def = (Player) e.getEntity();
					if (arrow.hasMetadata("Special")){
						if (arrow.getMetadata("Special").get(0).asString().equalsIgnoreCase("Freeze")){
							freezes.add(def.getName());
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
									freezes.remove(def.getName());
								}
							}, 10 * 20L);

						}
						else if (arrow.getMetadata("Special").get(0).asString().equalsIgnoreCase("Poison")){
							def.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 15*20,0));
						}
					}
				}
			}
		}
	}

	// Get players within a radius of a location
	public static HashMap<Player, Double> getPlayersAround(Location startLoc, int i) {
		HashMap<Player, Double> players = new HashMap<Player, Double> ();
		for (Player pl : Bukkit.getOnlinePlayers() ){
			if (pl.getWorld().getName() == startLoc.getWorld().getName())
				if (pl.getLocation().distance(startLoc) <= i){
					players.put(pl, pl.getLocation().distance(startLoc));
				}
		}
		return players;
	}







	@SuppressWarnings("deprecation")
	private Snowball launchDropSnowball(Player p, double d) {
		if (p.getItemInHand().getAmount() >= 1){
			if (p.getItemInHand().getAmount() >= 2)
				p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
			else
				p.setItemInHand(null);
			p.updateInventory();
			Snowball snowball = p.getWorld().spawn(p.getEyeLocation(), Snowball.class);
			snowball.setShooter(p);
			snowball.setVelocity(p.getLocation().getDirection().multiply(1.5));
			return snowball;
		}
		return null;
	}

	/*
	public static void OnProjectileLand(ProjectileHitEvent e){
		Entity ent = e.getEntity();
		if (ent instanceof Fish){
			Fish fishingHook = (Fish) ent;
			ProjectileSource shooter = fishingHook.getShooter();
			if (shooter instanceof Player){
				Player dam = ((Player) shooter).getPlayer();
				if (itemModifications.compareItems(dam.getItemInHand(), Material.FISHING_ROD, MessageSW.GrapplingHook)){

				}
			}
		}
	}
	 */
	@EventHandler
	public void onPFE(PlayerFishEvent e){
		Player dam = e.getPlayer();
		if (itemModifications.compareItems(dam.getItemInHand(), Material.FISHING_ROD, MessageSW.GrapplingHook)){
			if (e.getState() == State.IN_GROUND){
				pullEntityToLocation((Entity)e.getPlayer().getPlayer(), e.getHook().getLocation());
				Message.P(dam, MessageSW.UsedGrapplingHook, true);
			}
			else{
				Message.P(dam, MessageSW.GrapplingHooked, true);
			}
		}
	}
	private void pullEntityToLocation(Entity e, Location loc)
	{
		Location entityLoc = e.getLocation();

		entityLoc.setY(entityLoc.getY() + 0.5D);
		e.teleport(entityLoc);

		double g = -0.08D;
		double d = loc.distance(entityLoc);
		double t = d;
		double v_x = (1.0D + 0.07D * t) * (loc.getX() - entityLoc.getX()) / t;
		double v_y = (1.0D + 0.03D * t) * (loc.getY() - entityLoc.getY()) / t - 0.5D * g * t;
		double v_z = (1.0D + 0.07D * t) * (loc.getZ() - entityLoc.getZ()) / t;

		Vector v = e.getVelocity();
		v.setX(v_x);
		v.setY(v_y);
		v.setZ(v_z);
		e.setVelocity(v);
	}
	private boolean interactedWithSpecialSign(Player p, Block b) {
		if (ServerSignManagement.blockIsSign(b)){
			Sign sign = (Sign)b.getState();
			String arenaName = ChatColor.stripColor(sign.getLine(3).toLowerCase());
			if (Main.arenas.containsKey(arenaName)){
				JoinListener.autoJoin(p, arenaName);
				return true;
			}
		}
		return false;
	}
}
