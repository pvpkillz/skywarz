package kz.PvP.SkyWars.Events;

import java.sql.SQLException;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.GameStatus;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;

public class deathListener implements Listener{
	public static Main plugin;

	public deathListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	
    @EventHandler
    public void OnDamageEntity(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player){
            Player def = (Player) e.getEntity();
            if (e.getCause() == DamageCause.FALL)
                e.setCancelled(true);
            if(PlayerManagement.isPlayerGamer(def.getName()) && Main.arenas.get(PlayerManagement.getPlayerArena(def.getName())).getGameStatus().equals(GameStatus.WAITING))
            	e.setCancelled(true);
        }
    }
	
	
	@EventHandler
	public void OnInteractEvent (EntityDeathEvent e) throws SQLException{
		if (e.getEntity() instanceof Player){
			Player def = (Player) e.getEntity();
			
			if (PlayerManagement.isPlayerGamer(def.getName())){
				def.setHealth(20);
				def.getInventory().clear();
				for (PotionEffect pe : def.getActivePotionEffects())
					def.removePotionEffect(pe.getType());
				Main.arenas.get(PlayerManagement.getPlayerArena(def.getName())).removePlayer(def.getName(), true);
			}
		}
	}
	
	
	@EventHandler
	public void foodbarDrop (FoodLevelChangeEvent e){
		e.setCancelled(true);
		e.setFoodLevel(20);
	}
	
	
}
