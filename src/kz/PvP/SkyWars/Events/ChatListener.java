package kz.PvP.SkyWars.Events;

import java.sql.SQLException;
import java.util.logging.Logger;










import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import kz.PvP.SkyWars.Utilities.Files;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public ChatListener (Main mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);	
	}


	@EventHandler
	public void OnChatEvent (AsyncPlayerChatEvent e) throws SQLException{
		Player p = e.getPlayer();
		if (!e.isCancelled()){
			if (PlayerManagement.mapEditors.contains(p.getUniqueId())){
				if (!PlayerManagement.mapEditiorLink.containsKey(p.getUniqueId())){
					if (e.getMessage().equalsIgnoreCase("list")){
						PlayerManagement.displayArenasList(p);
					}
					else{
						if (Main.freshArenas.containsKey(e.getMessage()))
							Message.P(p, Message.Replacer(MessageSW.EditingMap, e.getMessage(), "%map"), true);
						else{
							Main.freshArenas.put(e.getMessage(), new arenaDetails(plugin));
							Message.P(p, Message.Replacer(MessageSW.CreatedNewMap, e.getMessage(), "%map"), true);
						}
						PlayerManagement.mapEditiorLink.put(p.getUniqueId(), e.getMessage());
					}
					e.setCancelled(true);
				}
				else if (e.getMessage().toLowerCase().startsWith("map")){
					if (PlayerManagement.mapEditiorLink.containsKey(p.getUniqueId())){
						if (Main.freshArenas.containsKey(PlayerManagement.mapEditiorLink.get(p.getUniqueId()))){
							Main.freshArenas.get(PlayerManagement.mapEditiorLink.get(p.getUniqueId())).setMapName(e.getMessage().split(" ")[1]);
						}
					}
					e.setCancelled(true);
				}
				else if (e.getMessage().toLowerCase().startsWith("name")){
					if (PlayerManagement.mapEditiorLink.containsKey(p.getUniqueId())){
						if (Main.freshArenas.containsKey(PlayerManagement.mapEditiorLink.get(p.getUniqueId()))){
							arenaDetails Details = Main.freshArenas.get(PlayerManagement.mapEditiorLink.get(p.getUniqueId()));
							Main.freshArenas.remove(PlayerManagement.mapEditiorLink.get(p.getUniqueId()));
							Details.setName(e.getMessage().split(" ")[1]);
							Main.freshArenas.put(e.getMessage().split(" ")[1], Details);
							Files.saveArenas();
							Main.setupArenas(plugin);
						}
						PlayerManagement.mapEditiorLink.put(p.getUniqueId(), e.getMessage().split(" ")[1]);
					}
					e.setCancelled(true);
				}
				else if (e.getMessage().equalsIgnoreCase("done")){
					PlayerManagement.mapEditors.remove(p.getUniqueId());
					PlayerManagement.mapEditiorLink.remove(p.getUniqueId());
					e.setCancelled(true);
				}
			}
		}
	}
}
