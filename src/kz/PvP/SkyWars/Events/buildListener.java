package kz.PvP.SkyWars.Events;


import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.GameStatus;
import kz.PvP.SkyWars.Methods.PlayerManagement;


public class buildListener implements Listener{
	public static Main plugin;

	public buildListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	@EventHandler
	public void OnBreakEvent (BlockBreakEvent e){
		Player p = e.getPlayer();
		if (PlayerManagement.isPlayerGamer(p.getName()))
			if(Main.arenas.get(PlayerManagement.getPlayerArena(p.getName())).getGameStatus().equals(GameStatus.WAITING))
				e.setCancelled(true);
	}

	@EventHandler
	public void OnBuildEvent (BlockPlaceEvent e){
		Player p = e.getPlayer();
		Block b = e.getBlock();
		if (PlayerManagement.isPlayerGamer(p.getName()))
			if((Main.arenas.get(PlayerManagement.getPlayerArena(p.getName()).toLowerCase()).getGameStatus().equals(GameStatus.WAITING)) || b.getLocation().getBlockY() >= Main.freshArenas.get(PlayerManagement.getPlayerArena(p.getName())).getSpawnpoints().get(0).getBlockY() + 8)
				e.setCancelled(true);
	}
}
