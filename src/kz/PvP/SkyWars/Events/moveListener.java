package kz.PvP.SkyWars.Events;

import kz.PvP.PkzAPI.methods.Locations;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.GameStatus;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class moveListener implements Listener{
	public static Main plugin;

	public moveListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}

	@EventHandler
	public void OnMoveEvent (PlayerMoveEvent e){
		if (!Locations.locationSameAs(e.getTo(), e.getFrom())){
			Player p = e.getPlayer();
			String arenaName = PlayerManagement.getPlayerArena(p.getName());
			if (arenaName != null){
				arenaDetails arena = Main.arenas.get(arenaName);
				if (arena.getGameStatus().equals(GameStatus.WAITING)){
					//e.setTo(e.getFrom());
				}
			}
		}
	}
}
