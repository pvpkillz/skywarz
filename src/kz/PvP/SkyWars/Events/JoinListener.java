package kz.PvP.SkyWars.Events;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Logger;




























import kz.PvP.PkzAPI.enums.PlayerScoreboard;
import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Enums.GameStatus;
import kz.PvP.SkyWars.Enums.arenaDetails;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import kz.PvP.SkyWars.Utilities.MessageSW;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.messaging.PluginMessageRecipient;

public class JoinListener implements Listener{
	public static Main plugin;
	

	
	
	static Logger log = Bukkit.getLogger();

	public JoinListener (Main mainclass){
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);	
	}

	@EventHandler
	public void quitEvent (PlayerQuitEvent e) {
		Player p = e.getPlayer();
		PlayerManagement.removePlayerEntries(p);
	}
	@EventHandler
	public void kickEvent (PlayerKickEvent e) {
		Player p = e.getPlayer();
		PlayerManagement.removePlayerEntries(p);
	}


	@EventHandler
	public void connectEvent (PlayerLoginEvent e){
		Player p = e.getPlayer();
		if (Main.openArenas.size() == 0){
			//p.teleport(Main.lobbyLoc);
			// We will later configure it to send player to another server if this one has no open arenas. But this is when we add more servers...
			
			/*
			e.disallow(Result.KICK_OTHER, MessageSW.noArenas);
			plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
			ByteArrayOutputStream b = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(b);
			try
			{
				out.writeUTF("reconnectSystem");
				out.writeUTF("tryAnother");
			}
			catch (IOException localIOException)
			{
			}

			((PluginMessageRecipient)p).sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
			 */
		}
	}

	@EventHandler
	public void JoinEvent (PlayerJoinEvent e) {
		Player p = e.getPlayer();
		resetPlayerStatus(p);
		autoJoin(p,"Random");
	}

	public static void resetPlayerStatus(Player p) {
		PlayersInfo.ClearFully(p);
		p.setFireTicks(0);
		p.setAllowFlight(false);
		p.setFlying(false);
		p.setGameMode(GameMode.SURVIVAL);
		p.setFoodLevel(20);
		p.setHealth(20.0);
		p.setExp(0);
		p.setTotalExperience(0);
		p.setLevel(0);
	}

	public static boolean autoJoin(Player p, String arenaNamePreset) {
		if (arenaNamePreset.equalsIgnoreCase("random")){
			if (Main.openArenas.size() >= 1){
				arenaDetails tempDetails = null;
				arenaDetails details = Main.arenas.get(Main.openArenas.get(ConvertTimings.randomInt(0, Main.openArenas.size() - 1)).toLowerCase());
				for (String arenaName : Main.openArenas){
					tempDetails = Main.arenas.get(arenaName.toLowerCase());
					if (tempDetails.getPlayers().size() >= 1){
						details = Main.arenas.get(arenaName.toLowerCase());
						if (details.addPlayer(p))
							return true;
					}
				}
				if (details.addPlayer(p))
					return true;
				else
					return false;
			}
			else{
				if (!PlayerManagement.queuedPlayers.contains(p.getName())){
					Message.P(p, MessageSW.noArenas, true);
					PlayerManagement.queuedPlayers.add(p.getName());
					p.teleport(Main.lobbyLoc);
				}
			}
		}
		else{
			if (Main.arenas.containsKey(arenaNamePreset.toLowerCase())){
				if (Main.arenas.get(arenaNamePreset.toLowerCase()).getGameStatus().equals(GameStatus.WAITING)){
					Main.arenas.get(arenaNamePreset.toLowerCase()).addPlayer(p);
					return true;
				}
				else
					Message.P(p, MessageSW.arenaStartedAlready, true);
			}
			else
				Message.P(p, MessageSW.arenaDoesNotExist, true);
		}
		return false;
	}
}
