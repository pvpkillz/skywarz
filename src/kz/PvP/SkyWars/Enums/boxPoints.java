package kz.PvP.SkyWars.Enums;

import java.util.List;

import kz.PvP.PkzAPI.methods.Locations;

import org.bukkit.Location;
import org.bukkit.block.Block;

public class boxPoints {
	private Location pointSel1 = null;
	private Location pointSel2 = null;

	
	public boxPoints(Location point1, Location point2){
		pointSel1 = point1;
		pointSel2 = point2;
	}
	
	
	public Location getPoint(Integer pointID){
		if (pointID == 1)
			return pointSel1;
		else if (pointID == 2)
			return pointSel2;
		else
			return null;
	}
	
	public void setPoint(Integer pointID, Location targetLoc){
		if (pointID == 1)
			pointSel1 = targetLoc;
		else if (pointID == 2)
			pointSel2 = targetLoc;
	}
	
	
	
	
	
	public boolean isLocInBox(Location pointCheck){
		return Locations.locationBetweenPoints(pointCheck, pointSel1, pointSel2);
	}
	public List<Block> getBlocksInBox(){
		return Locations.blocksFromTwoPoints(pointSel1, pointSel2, false);
	}
	
	
}
