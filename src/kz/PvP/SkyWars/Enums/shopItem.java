package kz.PvP.SkyWars.Enums;

import org.bukkit.inventory.ItemStack;

public class shopItem implements Cloneable{
	private String itemName = null;
	private String itemDesc = null;
	private Integer itemPrice = null;
	private ItemStack itemStack = null;
	private Integer itemPosition = null;
	
	public void setName(String name){
		if (name == null)
			name = "";
		itemName = name;
	}
	public void setDesc(String desc){
		if (desc != null && !desc.isEmpty())
			itemDesc = desc;
		else
			itemDesc = "No description available.";
	}
	public void setPrice(Integer cost){
		if (cost != null)
			itemPrice = cost;
		else
			itemPrice = 99999;
	}
	public void setItemStack(ItemStack is){
		itemStack = is;
	}
	public void setPosition(Integer pos){
		itemPosition = pos;
	}
	public String getName(){
		return itemName;
	}
	public String getDesc(){
		return itemDesc;
	}
	public Integer getPrice(){
		return itemPrice;
	}
	public ItemStack getItem(){
		return itemStack;
	}
	public Integer getPosition(){
		return itemPosition;
	}

}
