package kz.PvP.SkyWars.Enums;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import kz.PvP.PkzAPI.methods.ConvertTimings;
import kz.PvP.PkzAPI.methods.PlayersInfo;
import kz.PvP.PkzAPI.methods.Scoreboards;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;
import kz.PvP.PkzAPI.utilities.Stats;
import kz.PvP.SkyWars.Main;
import kz.PvP.SkyWars.Events.JoinListener;
import kz.PvP.SkyWars.Methods.ChangeMaps;
import kz.PvP.SkyWars.Methods.PlayerManagement;
import kz.PvP.SkyWars.Utilities.Files;
import kz.PvP.SkyWars.Utilities.MessageSW;
import kz.PvP.SkyWars.repeatingTasks.waitingTimer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class arenaDetails implements Cloneable{
	static Main plugin = null;

	public arenaDetails(Main mainclass){
		// Nothing :D
		plugin = mainclass;
	}
	/*
	public arenaDetails (arenaDetails detailsNeeded) {
		System.out.println("Copied the variable... " + detailsNeeded.getMapName());
		this.mapDroppers = new ArrayList<Location> (mapDroppers);
		this.status = getGameStatus();
		this.mapName = getMapName();
		this.arenaName = getName();
		this.waitingTime = getWaitTime();
		this.mapSpawnpoints = new ArrayList<Location> (mapSpawnpoints);
		System.out.println("Copied the variable... " + this.getMapName());
	}
*/
	@Override
	public arenaDetails clone() {
		arenaDetails details = new arenaDetails(plugin);
		//Deep copy
		details.mapDroppers = new ArrayList<Location> (mapDroppers);
		details.status = getGameStatus();
		details.mapName = getMapName();
		details.arenaName = getName();
		details.waitingTime = getWaitTime();
		details.mapSpawnpoints = new ArrayList<Location> (mapSpawnpoints);
		return details;
	}


	String mapName = "Default";
	String arenaName = "Default";
	GameStatus status = GameStatus.WAITING;
	ArrayList<Location> mapSpawnpoints = new ArrayList<Location>();
	ArrayList<Location> mapDroppers = new ArrayList<Location>();
	Integer waitingTime = Main.waitTime;//Time for waiting
	Integer arenaDuration = 0;//Time of game so far..
	ArrayList<String> contestants = new ArrayList<String>();
	HashMap<String, Location> usedSpawnpoints = new HashMap<String, Location>();
	BukkitTask waitingTimer = null;
	ArrayList<String> originalContestants = new ArrayList<String>();
	boolean enabledArena = false;

	public void setEnabled(boolean enable){
		enabledArena = enable;
	}
	public boolean getEnabled(){
		return enabledArena;
	}
	public void setGameStatus(GameStatus Status){
		status = Status;
	}

	public GameStatus getGameStatus(){
		return status;
	}
	public ArrayList<String> getOriginal(){
		return originalContestants;
	}

	public void setMapName(String name){
		mapName = name;
	}

	public void setWaitTime(Integer wait){
		waitingTime = wait;
	}
	public Integer getWaitTime (){
		return waitingTime;
	}

	public void setName(String name){
		arenaName = name;
	}
	public String getName(){
		return arenaName;
	}

	public String getMapName(){
		return mapName;
	}


	public void setSpawnpoints(ArrayList<Location> locations){
		mapSpawnpoints = locations;
	}
	public void setDroppers(ArrayList<Location> locations){
		mapDroppers = locations;
	}


	public void removeSpawnpoint(Location loc){
		mapSpawnpoints.remove(loc);
	}
	public void removeDropper(Location loc){
		mapDroppers.remove(loc);
	}


	public boolean addPlayer(Player p){
		// A player joined the game! :D
		if (p != null){
			p.getInventory().clear();
			p.setHealth(20.0);
			p.setFoodLevel(20);
			if (status.equals(GameStatus.WAITING) && mapSpawnpoints.size() >= 1){
				contestants.add(p.getName());
				Main.globalGamers.put(p.getName(), getName());
				Scoreboards.scoreboardsList.remove(p.getName());
				Scoreboards.getBoard(p.getName());
				if (contestants.size() >= 1)
				if (waitingTimer == null || !Bukkit.getScheduler().isCurrentlyRunning(waitingTimer.getTaskId()))
						waitingTimer = new waitingTimer(arenaName.toLowerCase()).runTaskTimer(plugin, 1 * 20L, 1 * 20L);// I hope this works XD
				
				
				Message.P(p, MessageSW.rackUpShop, true);
				MessageSW.AP(arenaName, Message.Replacer(MessageSW.joinedTheArena, p.getName(), "%player"), true);
				int random = ConvertTimings.randomInt(0, mapSpawnpoints.size() - 1);
				p.teleport(mapSpawnpoints.get(random));
				usedSpawnpoints.put(p.getName(), mapSpawnpoints.get(random));
				mapSpawnpoints.remove(random);
				Scoreboards.scoreboardsList.remove(p.getName());
				Scoreboards.getBoard(p.getName());
				originalContestants = (ArrayList<String>) contestants.clone();
				return true;
			}
		}
		return false;
	}

	public boolean removePlayer(String playerName, boolean newArena){
		// A player joined the game! :D
		if (contestants.contains(playerName)){
			contestants.remove(playerName);
			Main.globalGamers.remove(playerName);
			Player p = Bukkit.getPlayer(playerName);
			if (!status.equals(GameStatus.WAITING)){
				if (contestants.size() >= 1){// Too bad he lost.
					MessageSW.AP(arenaName, Message.Replacer(MessageSW.Eliminated, playerName, "%player"), true);
				}
				if (contestants.size() == 1){// We got a winner
					gameEnded();
				}
			}
			else{
				originalContestants = (ArrayList<String>) contestants.clone();
				mapSpawnpoints.add(usedSpawnpoints.get(playerName));
				usedSpawnpoints.remove(playerName);
			}
			if (p != null && p.isOnline()){
				if (newArena)
					JoinListener.autoJoin(p, "Random");
				else
					p.teleport(Main.lobbyLoc);
				Scoreboards.scoreboardsList.remove(playerName);
				Scoreboards.getBoard(playerName);
			}
			JoinListener.resetPlayerStatus(p);
		}
		return false;
	}


	public void gameEnded() {
		Player winner = PlayersInfo.getPlayer(contestants.get(0));
		Message.G(Message.Replacer(Message.Replacer(MessageSW.WonTheGame,getMapName(), "%map"),winner.getName(), "%winner"), true);
		if (originalContestants.size() >= 4){
			try {
				Mysql.modifyTokens(winner.getName(), 4, true);
			} catch (SQLException e) {e.printStackTrace();}
		}
		else
			Message.P(winner, MessageSW.inviteMoreForReward, true);
		JoinListener.autoJoin(winner, "Random");
		JoinListener.resetPlayerStatus(winner);
		Stats.killStreaks.remove(winner.getName());
		contestants.clear();
		mapDroppers.clear();
		originalContestants.clear();
		status = GameStatus.WAITING;
		waitingTime = Main.waitTime;
		regenerateMap();
		Main.openArenas.add(arenaName.toLowerCase());
		PlayerManagement.setupQueue();
	}


	public void gameStarted() {
		MessageSW.AP(arenaName, MessageSW.GameStarted, true);
		setGameStatus(GameStatus.STARTED);
		Main.openArenas.remove(arenaName.toLowerCase());
		for (Location loc : mapDroppers){
			loc.getBlock().setType(Material.AIR);
			loc.getBlock().getState().update();
		}
		originalContestants = (ArrayList<String>) contestants.clone();
	}


	public void addSpawnpoint(Location loc){
		mapSpawnpoints.add(loc);
	}
	public void addDropper(Location loc){
		mapDroppers.add(loc);
	}



	public ArrayList<Location> getSpawnpoints(){
		return mapSpawnpoints;
	}
	public ArrayList<Location> getDroppers(){
		return mapDroppers;
	}
	public int getDuration(){
		return arenaDuration;
	}

	public ArrayList<String> getPlayers(){
		ArrayList<String> players = new ArrayList<String>();
		for (String playerName : contestants)
			players.add(playerName);
		return players;
	}


	public void regenerateMap(){
		ChangeMaps.LoadMapSave(mapName);
		mapSpawnpoints = Main.freshArenas.get(arenaName.toLowerCase()).clone().mapSpawnpoints;
		for (Location loc : mapSpawnpoints){
			loc.setWorld(Bukkit.getWorld(mapName));
			for (int x = -1; x <= 1; x++){
				for (int y = -1; y <= 2; y++){
					for (int z = -1; z <= 1; z++){
						if (x == 0 && z == 0 && (y == 0 || y == 1 || y == 2))
							loc.clone().add(x,y,z).getBlock().setType(Material.AIR);
						else{
							addDropper(loc.clone().add(x,y,z));
							loc.clone().add(x,y,z).getBlock().setType(Material.GLASS);
						}
					}
				}
			}
		}
	}



}
